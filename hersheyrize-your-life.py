#!/usr/bin/env python2

'''
Copyright (C) 2018 Luuse, contact@luuse.io

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
'''

import inkex, cubicsuperpath, simplepath, cspsubdiv, math
import lxml.etree as ET

class MyEffect(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("-f", "--flatness",
                        action="store", type="float",
                        dest="flat", default=10.0,
                        help="Minimum flatness of the subdivided curves")
    def effect(self):
        for id, node in self.selected.iteritems():
            if node.tag == inkex.addNS('path','svg'):
                d = node.get('d')
                p = cubicsuperpath.parsePath(d)
                cspsubdiv.cspsubdiv(p, self.options.flat)
                np = []
                for sp in p:
                    first = True
                    for csp in sp:
                        cmd = ' L '
                        if first:
                            cmd = ' M '
                        first = False
                        np.append([cmd,[csp[1][0],csp[1][1]]])
                pth = simplepath.formatPath(np)
                pthS = pth.split(' ')
                newPath = []
                for elemp in pthS:

                    if elemp.find('.') != -1 and elemp.find(',') != -1:
                        v = elemp.split(',')
                        newPath += str(round(float(v[0])))
                        newPath += ','
                        newPath += str(round(float(v[1])))

                    elif elemp.find('.') != -1:
                        newPath += str(round(float(elemp)))

                    else:
                        newPath += str(elemp)

                    newPath += ' '

                node.set('d',''.join(newPath))


if __name__ == '__main__':
    e = MyEffect()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
